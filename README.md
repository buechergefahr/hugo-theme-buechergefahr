# hugo-theme-buechergefahr

Theme for the homepage at [https://buechergefahr.de](https://buechergefahr.de). Inspired by [castanet](https://github.com/mattstratton/castanet) and uses the [strongly typed](https://html5up.net/strongly-typed) theme from [HTML5 UP](http://html5up.net).

## Use

```console
% git submodule add https://codeberg.org/buechergefahr/hugo-theme-buechergefahr.git themes/buechergefahr
```

In `config.toml`:

```toml
theme = "buechergefahr"
```

## Note

This uses a clone of the [podigee podcast player](https://github.com/podigee/podigee-podcast-player/tree/master/src) in `static/podigee-podcast-player` which one could update occasionally, depending on personal preference:

```console
cd ...3rdParty/podigee-podcast-player
yarn install
gulp build
```
Than copy the generated assets to `static/podigee-podcast-Player `.

## See also

* [castanet](https://github.com/mattstratton/castanet)
* [strongly typed](https://html5up.net/strongly-typed)
* [HTML5 UP](http://html5up.net)
* [Podigee Podcast Player](https://github.com/podigee/podigee-podcast-player)
